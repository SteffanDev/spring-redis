# spring-redis

**Redis** is an open source (BSD licensed), in-memory data 
structure store, used as a database, cache and message broker.
It supports data structures such as strings, hashes, lists, sets,
 sorted sets with range queries, bitmaps, hyperloglogs, geospatial indexes with radius queries and streams.
 
 ### Jedis - Client library in Java for Redis 
 - Jedis is a blazingly small and sane Redis java client.
 - Jedis was conceived to be EASY to use.
 - Jedis is fully compatible with redis 2.8.x, 3.x.x and above*.

